### test 2

Testing the -p option.

#### Run the test:

```
npm run test2
```

or:

```
./bin/c-watch -s "./test/2/Makefile,./test/2/*" -o "test2" -p "./test/2/foo.txt"
```
